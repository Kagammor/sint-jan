<?php include '../header.php'; ?>
<h1 class="header">Tweetalig Onderwijs (TTO)</h1>
<p>TweeTalig Onderwijs bieden we op Sint-Jan sinds 1 augustus 2010. TTO wordt gegeven op het vwo, zowel op het gymnasium als het atheneum.</p>

<br />

<p>Aan de landelijke eis dat minimaal 50% van de lessen in het Engels moet worden aangeboden, voldoen we ruim. In de brugklas en de 2e klas bieden we ongeveer 2/3 van de lessen in het Engels aan:
	<ul class="list">
		<li>English <span class="title_sub">(Engels)</span></li>
		<li>Extra English</li>
		<li>History <span class="title_sub">(Geschiedenis)</span></li>
		<li>Geography <span class="title_sub">(Aardrijkskunde)</span></li>
		<li>Mathematics <span class="title_sub">(Wiskunde)</span></li>
		<li>Economics <span class="title_sub">(Economie)</span></li>
		<li>Science <span class="title_sub">(Mens & Natuur)</span></li>
		<li>Art <span class="title_sub">(Beeldende Vormgeving)</span></li>
		<li>IT studies <span class="title_sub">(Informatica)</span></li>
		<li>Physical education <span class="title_sub">(Lichamelijke opvoeding)</span></li>
	</ul>
</p>

<br />

<p>Door TTO willen we de leerlingen een (nog) betere aansluiting bieden met het vervolgonderwijs op de universiteiten en hogescholen, waar Engels steeds meer de voertaal is. 
Wil je eens zien hoe TTO er uitziet op Sint-Jan en hoe leerlingen erover denken, klik dan hier voor een filmpje. </p>
<?php include '../footer.php'; ?>
