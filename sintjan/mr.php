<?php include '../header.php'; ?>
<h1 class="header">Medezeggenschapsraad</h1>
<p>De medezeggenschapsraad is een wettelijk orgaan bestaande uit vertegenwoordigers van het personeel, de ouders en/of de leerlingen. Elke gewone school heeft verplicht een medezeggenschapsraad die meebeslist over de organisatie van de school en de inrichting van het onderwijs.</p>
<br />
<p>De medezeggenschapsraad van het Sint-Janscollege heeft een eigen website waar veel informatie te vinden is over o.a. de samenstelling van de raad en het recht van advies en het recht van instemming.<br />
Deze website is <a href="http://www.stjan-mr.nl/" target="_blank">hier</a> te vinden.</p>
<?php include '../footer.php'; ?>
