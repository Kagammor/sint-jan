<?php include '../header.php'; ?>
<h1 class="header">Informatie</h1>

<p>Sint-Jan is een school voor middelbaar havo, tweetalig vwo (Engels), atheneum en gymasium onderwijs.</p>

<br />

<h2 class="title">Schoolgrootte</h2>
<p>In het schooljaar 2011-2012 zijn de leerlingen als volgt verdeeld over de afdelingen:</p>
<table class="table_third">
	<tr>
		<td><h3 class="title_2nd">Brugklas</h3></td>
		<td><h3 class="title_2nd">Havo</h3></td>
		<td><h3 class="title_2nd">Vwo</h3></td>
		<td><h3 class="title_2nd">Totaal</h3></td>
	</tr>
	<tr>
		<td>243</td>
		<td>465</td>
		<td>520</td>
		<td><h2 class="title_3rd">1228</h2></td>
	</tr>
</table>
<br />
<p>Sint-Jan telt in schooljaar 2011-2012 ongeveer 120 personeelsleden.</p>

<br />

<p>Het totaal aantal leerlingen in voorgaande schooljaren:</p>
<table class="table_full">
	<tr>
		<td><h3 class="title_2nd">2005-2006</h3></td>
		<td><h3 class="title_2nd">2006-2007</h3></td>
		<td><h3 class="title_2nd">2007-2008</h3></td>
		<td><h3 class="title_2nd">2008-2009</h3></td>
		<td><h3 class="title_2nd">2009-2010</h3></td>
		<td><h3 class="title_2nd">2010-2011</h3></td>
	</tr>
	<tr>
		<td>
			1478<br />
			(afbouw vmbo-t)
		</td>
		<td>
			1409<br />
			(afbouw vmbo-t)
		</td>
		<td>
			1350<br />
			(afbouw vmbo-t)
		</td>
		<td>
			1230<br />
			(afbouw vmbo-t)
		</td>
		<td>1196</td>
		<td>1200</td>
	</tr>
</table>

<br />

<h2 class="title">Lessentabellen</h2>
<p>De gegevens in de lessentabellen zijn het aantal uren per vak aangegeven per leerjaar en -niveau.</p>

<br />

<h3 class="title_2nd">Onderbouw</h3>
<table class="table_tf">
	<tr>
		<td><h3 class="title_3rd">Vak</h3></td>
		<td><h3 class="title_3rd">Brugklas</h3></td>
		<td><h3 class="title_3rd">H2/A2</h3></td>
		<td><h3 class="title_3rd">H3/A3</h3></td>
		<td><h3 class="title_3rd">G2</h3></td>
		<td><h3 class="title_3rd">G3</h3></td>
	</tr>
	<tr>
		<td>Nederlands</td>
		<td>4</td>
		<td>3</td>
		<td>3</td>
		<td>3</td>
		<td>3</td>
	</tr>
	<tr>
		<td>Latijn</td>
		<td>1<sup>*1</sup></td>
		<td>-</td>
		<td>-</td>
		<td>2</td>
		<td>3</td>
	</tr>
	<tr>
		<td>Grieks</td>
		<td>-</td>
		<td>-</td>
		<td>-</td>
		<td>2</td>
		<td>3</td>
	</tr>
	<tr>
		<td>Frans</td>
		<td>3</td>
		<td>3</td>
		<td>3</td>
		<td>2</td>
		<td>3</td>
	</tr>
	<tr>
		<td>Duits</td>
		<td>-</td>
		<td>3</td>
		<td>3</td>
		<td>3</td>
		<td>3</td>
	</tr>
	<tr>
		<td>Engels/English</td>
		<td>3</td>
		<td>3</td>
		<td>3</td>
		<td>2</td>
		<td>2</td>
	</tr>
	<tr>
		<td>Extra English</td>
		<td>1<sup>*2</sup></td>
		<td>1<sup>*2</sup></td>
		<td>-</td>
		<td>2<sup>*2</sup></td>
		<td>-</td>
	</tr>
	<tr>
		<td>Geschiedenis/History</td>
		<td>2</td>
		<td>2</td>
		<td>2</td>
		<td>2</td>
		<td>2</td>
	</tr>
	<tr>
		<td>Aardrijkskunde/Geography</td>
		<td>3</td>
		<td>3</td>
		<td>3</td>
		<td>2</td>
		<td>2</td>
	</tr>
	<tr>
		<td>Wiskunde/Mathematics</td>
		<td>3</td>
		<td>4</td>
		<td>4</td>
		<td>4</td>
		<td>3</td>
	</tr>
	<tr>
		<td>Natuurkunde</td>
		<td>-</td>
		<td>-</td>
		<td>3</td>
		<td>-</td>
		<td>3</td>
	</tr>
	<tr>
		<td>Scheikunde</td>
		<td>-</td>
		<td>-</td>
		<td>2</td>
		<td>-</td>
		<td>2</td>
	</tr>
	<tr>
		<td>Biologie</td>
		<td>-</td>
		<td>-</td>
		<td>2</td>
		<td>-</td>
		<td>2</td>
	</tr>
	<tr>
		<td>Economie/Economics</td>
		<td>-</td>
		<td>1</td>
		<td>1</td>
		<td>1</td>
		<td>1</td>
	</tr>
	<tr>
		<td>Beeldende Vormgeving/Art</td>
		<td>2</td>
		<td>2</td>
		<td>1</td>
		<td>2</td>
		<td>-</td>
	</tr>
	<tr>
		<td>Levensbeschouwing</td>
		<td>2</td>
		<td>1</td>
		<td>-</td>
		<td>1</td>
		<td>-</td>
	</tr>
	<tr>
		<td>Muziek</td>
		<td>1</td>
		<td>1</td>
		<td>1</td>
		<td>1</td>
		<td>-</td>
	</tr>
	<tr>
		<td>Mens & Natuur/Science</td>
		<td>4</td>
		<td>4</td>
		<td>-</td>
		<td>4</td>
		<td>-</td>
	</tr>
	<tr>
		<td>Informatica/IT studies</td>
		<td>1</td>
		<td>-</td>
		<td>-</td>
		<td>-</td>
		<td>-</td>
	</tr>
	<tr>
		<td>Lich. Opvoeding/Physical education</td>
		<td>4</td>
		<td>2</td>
		<td>2</td>
		<td>2</td>
		<td>2</td>
	</tr>
	<tr>
		<td>Studieles/Study hour</td>
		<td>1</td>
		<td>1</td>
		<td>1</td>
		<td>1<sup>*3</sup></td>
		<td>1</td>
	</tr>
	<tr>
		<td><h3 class="title_3rd">Totaal</h3></td>
		<td><h2 class="title_3rd">32</h2></td>
		<td><h2 class="title_3rd">32</h2></td>
		<td><h2 class="title_3rd">33</h2></td>
		<td><h2 class="title_3rd">33</h2></td>
		<td><h2 class="title_3rd">35</h2></td>
	</tr>
</table>
<br />
<footer class="notes">
	<ul>
		<li><sup>*1</sup> In de brugklas wordt Latijn alleen gegeven in de gymnasiumbrugklassen.</li>
		<li><sup>*2</sup> Extra English wordt alleen gegeven in de TTO-klassen.</li>
		<li><sup>*3</sup> In gymnasium-2 TTO is geen ingeroosterde studieles</li>
	</ul>
</footer>

<br />

<h3 class="title_2nd">Bovenbouw</h3>
Het vakkenpakket van de bovenbouw bestaat uit drie delen:
<ul class="list">
	<li><span class="title_3rd">Algemeen deel:</span> verplicht voor alle leerlingen</li>
	<li><span class="title_3rd">Profiel deel:</span> vier met elkaar samenhangende vakken</li>
	<li><span class="title_3rd">Vrije deel:</span> &eacute;&eacute;n extra beschikbaar vak naar keuze</li>
</ul>
<br />
Zie bladzijde 14 en 15 van de schoolgids Sint-Jan voor de precieze inrichting van de vakkenkeuze.

<br /><br />

<table class="table_tf">
	<tr>
		<td><h3 class="title_3rd">Vak</h3></td>
		<td><h3 class="title_3rd">H4</h3></td>
		<td><h3 class="title_3rd">H5</h3></td>
		<td><h3 class="title_3rd">V4</h3></td>
		<td><h3 class="title_3rd">V5</h3></td>
		<td><h3 class="title_3rd">V6</h3></td>
	</tr>
	<tr>
		<td>Nederlands</td>
		<td>4</td>
		<td>4</td>
		<td>3</td>
		<td>3</td>
		<td>3</td>
	</tr>
	<tr>
		<td>Engels</td>
		<td>4</td>
		<td>4</td>
		<td>2</td>
		<td>3</td>
		<td>4</td>
	</tr>
	<tr>
		<td>Latijn</td>
		<td>-</td>
		<td>-</td>
		<td>3</td>
		<td>4</td>
		<td>5</td>
	</tr>
	<tr>
		<td>Grieks</td>
		<td>-</td>
		<td>-</td>
		<td>3</td>
		<td>4</td>
		<td>5</td>
	</tr>
	<tr>
		<td>Frans</td>
		<td>4</td>
		<td>4</td>
		<td>3</td>
		<td>3</td>
		<td>3</td>
	</tr>
	<tr>
		<td>Duits</td>
		<td>4</td>
		<td>4</td>
		<td>3</td>
		<td>3</td>
		<td>3</td>
	</tr>
	<tr>
		<td>Geschiedenis</td>
		<td>3</td>
		<td>3</td>
		<td>3</td>
		<td>3</td>
		<td>3</td>
	</tr>
	<tr>
		<td>Aardrijkskunde</td>
		<td>3</td>
		<td>3</td>
		<td>2</td>
		<td>3</td>
		<td>3</td>
	</tr>
	<tr>
		<td>Economie</td>
		<td>4</td>
		<td>4</td>
		<td>3</td>
		<td>3</td>
		<td>3</td>
	</tr>
	<tr>
		<td>Biologie</td>
		<td>4</td>
		<td>4</td>
		<td>3</td>
		<td>3</td>
		<td>3</td>
	</tr>
	<tr>
		<td>Natuurkunde</td>
		<td>4</td>
		<td>4</td>
		<td>3</td>
		<td>3</td>
		<td>3</td>
	</tr>
	<tr>
		<td>Scheikunde</td>
		<td>3</td>
		<td>4</td>
		<td>2</td>
		<td>3</td>
		<td>3</td>
	</tr>
	<tr>
		<td>Wiskunde A</td>
		<td>3</td>
		<td>4</td>
		<td>4</td>
		<td>3</td>
		<td>4</td>
	</tr>
	<tr>
		<td>Wiskunde B</td>
		<td>3</td>
		<td>4</td>
		<td>5</td>
		<td>4</td>
		<td>4</td>
	</tr>
	<tr>
		<td>Wiskunde C</td>
		<td>-</td>
		<td>-</td>
		<td>4</td>
		<td>3</td>
		<td>3</td>
	</tr>
	<tr>
		<td>Wiskunde D</td>
		<td>3</td>
		<td>3</td>
		<td>2</td>
		<td>3</td>
		<td>3</td>
	</tr>
	<tr>
		<td>KCV</td>
		<td>-</td>
		<td>-</td>
		<td>1</td>
		<td>2</td>
		<td>-</td>
	</tr>
	<tr>
		<td>CKV</td>
		<td>2</td>
		<td>-</td>
		<td>1</td>
		<td>2</td>
		<td>-</td>
	</tr>
	<tr>
		<td>Lich. opvoeding</td>
		<td>2</td>
		<td>1</td>
		<td>2</td>
		<td>2</td>
		<td>1</td>
	</tr>
	<tr>
		<td>BSM</td>
		<td>3</td>
		<td>3</td>
		<td>4</td>
		<td>3</td>
		<td>-</td>
	</tr>
	<tr>
		<td>Maatschappijleer</td>
		<td>2</td>
		<td>-</td>
		<td>2</td>
		<td>-</td>
		<td>-</td>
	</tr>
	<tr>
		<td>ANW</td>
		<td>-</td>
		<td>-</td>
		<td>2</td>
		<td>-</td>
		<td>-</td>
	</tr>
	<tr>
		<td>Levensbeschouwing</td>
		<td>-</td>
		<td>-</td>
		<td>-</td>
		<td>1</td>
		<td>-</td>
	</tr>
	<tr>
		<td>Kunst Algemeen/Kunst BV</td>
		<td>1/2</td>
		<td>1/3</td>
		<td>1/2</td>
		<td>1/2</td>
		<td>1/2</td>
	</tr>
	<tr>
		<td>Kunst Algemeen/Kunst Muziek</td>
		<td>1/2</td>
		<td>1/2</td>
		<td>1/2</td>
		<td>1/2</td>
		<td>1/2</td>
	</tr>
	<tr>
		<td>NLT</td>
		<td>3</td>
		<td>3</td>
		<td>-</td>
		<td>4</td>
		<td>4</td>
	</tr>
</table>
<?php include '../footer.php'; ?>
