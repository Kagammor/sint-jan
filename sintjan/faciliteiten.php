<?php include '../header.php'; ?>
<h1 class="header">Faciliteiten</h1>

<h2 class="title">Boekenfonds</h2>
<p>De leerlingen van Sint-Jan lenen de boeken die ze dagelijks in de lessen nodig hebben. De ouders sluiten met de school hiervoor een gebruikersovereenkomst. U vindt een voorbeeld hiervan op pagina 17 en 18 van het algemene deel van de schoolgids.</p>
<br />
<p>In het pakket dat de leerlingen ontvangen zitten naast de leerboeken ook de werkboeken, woordenboeken en (indien van toepassing) een atlas.</p>
<br />
<p>Van de leerlingen wordt verwacht dat zij zorgvuldig omgaan (o.a. kaften) met het boekenpakket. Raken boeken beschadigd of gaan ze verloren, dan zal hiervoor een vergoeding gevraagd worden.</p>

<br />

<h2 class="title">Ouderbijdrage</h2>
<p>De school vraagt de ouders mee te betalen aan bepaalde voorzieningen, de zogenaamde ouderbijdrage. Deze ouderbijdrage is vrijwillig. U kunt er als ouder voor kiezen of u wel of niet betaalt voor een bepaalde voorziening. Als u niet bijdraagt, kan de school uw kind uitsluiten van die bepaalde voorziening. Indien van toepassing wordt dan een vervangend programma aangeboden aan uw kind, dat hij/zij verplicht is te volgen.</p>
<br />
<p>Ook voor de ouderbijdrage sluiten de ouders en de school een overeenkomst. U vindt een voorbeeld hiervan op pagina 25 en 26 van het algemene deel van de schoolgids. Na ondertekening van de overeenkomst bent u verplicht de bijdrage te betalen. De hoogte en de wijze van besteding van deze gelden is met instemming van het ouders/leerlingendeel van de medezeggenschapsraad tot stand gekomen.</p>
<br />
De ouderbijdrage voor het schooljaar 2011-2012 varieert per leerjaar en afdeling. Een overzicht van deze bedragen vindt u op de pagina's 27 tot en met 34 van het algemene deel van de schoolgids. Voor leerlingen die TTO-onderwijs volgen, is de bijdrage &euro;305,00. Deze bijdrage is verplicht.</p>
<br />
<p>Indien meer kinderen uit een gezin onderwijs volgen op een van de scholen die samenwerken binnen LVO Parkstad, is een reductieregeling van toepassing (zie hiervoor ook de overeenkomst). Deze zal in het najaar worden verrekend.</p>

<br />

<h2 class="title">Kluisjes</h2>
<p>Leerlingen kunnen op Sint-Jan een kluisje huren. De kosten hiervan bedragen &euro;10,- huur per jaar en &euro;5,- borg. Bij het inleveren van de sleutel ontvangt de leerling de borg terug.</p>
<br />
<p>Kluisjes kunnen bij de administratie worden gehuurd. De ouders/verzorgers dienen hiervoor een contract in te vullen en te ondertekenen.</p>

<br />

<h2 class="title">Proefwerkblokken</h2>
<p>Aan het begin van het schooljaar ontvangt elke leerling een lijntjes- en een ruitjesblok. Hiervoor is betaald via de vrijwillige ouderbijdrage. Indien er in de loop van het schooljaar behoefte is aan een extra proefwerkblok, dan kan deze bij de administratie worden gekocht voor &euro;1,50.</p>
<br />
<p>Examenpapier voor de schoolexamens en het centraal examen worden gratis ter beschikking gesteld.</p>

<br />

<h2 class="title">L.O. T-shirt</h2>
<p>De leerlingen van de klassen 1, 3 en vwo-5 ontvangen een T-shirt voor lichamelijke opvoeding via de vakdocent. Voor dit shirt is betaald via de vrijwillige ouderbijdrage.</p>
<br />
<p>Leerlingen van de andere leerjaren en leerlingen die een tweede shirt willen hebben, kunnen dit kopen bij de administratie voor &euro;6,-.</p>
<?php include '../footer.php'; ?>
