<?php include '../header.php'; ?>
<h1 class="header">Personeel</h1>

<h2 class="title">Schoolleiding</h2>
<h3 class="title_2nd">Unitdirecteur</h3>
Dhr. H. Hansen<br />
tel. 046-4858123 | e-mail: h.hansen@sintjan.lvo.nl
<br /><br />
<h3 class="title_2nd">Sectordirecteur</h3>
Dhr. G. Zwitserloot<br />
tel. 045-5225541 | e-mail: g.zwitserloot@sintjan-lvo.nl</li>

<br /><br />

<h3 class="title_2nd">Teamleiders</h3>
<ul class="list">
<li><h4 class="title_3rd">Brugklas</h4>
Dhr. I. Heinemans<br />
tel. 06-31325179 | e-mail: i.heinemans@sintjan-lvo.nl</li>
<li><h4 class="title_3rd">Havo 2 en 3</h4>
Mvr. M van der Wal<br />
tel. 06-48593165 | e-mail: m.vanderwal@sintjan-lvo.nl</li>
<li><h4 class="title_3rd">VWO 2 en 3</h4>
Dhr. R. Puts<br />
tel. 046-8500132 | e-mail: r.puts@sintjan-lvo.nl</li>
<li><h4 class="title_3rd">Havo 4 en 5</h4>
Dhr. E. Reijnders<br />
tel. 045-5111613 | e-mail: e.reijnders@sintjan-lvo.nl</li>
<li><h4 class="title_3rd">VWO 4, 5 en6</h4>
Dhr. L. Severens<br />
tel. 045-5225541 | e-mail: l.severens@sintjan-lvo.nl</li>
</ul>

<br />

<h2 class="title">Onderwijzend personeel</h2>
<br>
<table class="table_full">
	<tr>
		<td><h3 class="title_2nd">Naam</h3></td>
		<td><h3 class="title_2nd">Initialen</h3></td>
		<td><h3 class="title_2nd">Afgekort</h3></td>
		<td><h3 class="title_2nd">Vakken</h3></td>
		<td><h3 class="title_2nd">E-mail</h3></td>
	</tr>
	<tr>
		<td>Ackermans</td>
		<td>mevr. L.M.</td>
		<td>acke</td>
		<td>Nederlands</td>
		<td>l.ackermans@sintjan-lvo.nl</td>
	</tr>
	<tr>
		<td>Baggen</td>
		<td>dhr. L.H.F.</td>
		<td>bagg</td>
		<td>Duits</td>
		<td>l.baggen@sintjan-lvo.nl</td>
	</tr>
	<tr>
		<td colspan="5" style="text-align: center;">~</td>
	</tr>
	<tr>
		<td>Wielens</td>
		<td>mevr. J.</td>
		<td>wiel</td>
		<td>
			Beeldende Vormgeving<br />
			(extra) Engels
		</td>
		<td>j.wielens@sintjan-lvo.nl</td>
	</tr>
	<tr>
		<td>Wolvekamp-Kleuters</td>
		<td>mevr. H.M.J.</td>
		<td>wolv</td>
		<td>
			Geschiedenis<br />
			Aardrijkskunde
		</td>
		<td>b.wolvekamp@sintjan-lvo.nl</td>
	</tr>
</table>

<br />

<h2 class="title">Onderwijsondersteunend personeel</h2>
<p>Het onderwijsondersteunend personeel is bereikbaar via het algemene e-mail adres van de school: info@sintjan-lvo.nl</p>
<table class="table_full">
	<tr>
		<td><h3 class="title_2nd">Naam</h3></td>
		<td><h3 class="title_2nd">Initialen</h3></td>
		<td><h3 class="title_2nd">Functie</h3></td>
	</tr>
	<tr>
		<td><h3>Bos</td>
		<td><h3>dhr. R.P.</td>
		<td><h3>Absentenregistratie, toezicht</td>
	</tr>
	<tr>
		<td><h3>Bosch</td>
		<td><h3>dhr. J.</td>
		<td><h3>Toezicht, onderhoudsmedewerker</td>
	</tr>
	<tr>
		<td colspan="3" style="text-align: center;">~</td>
	</tr>
	<tr>
		<td><h3>Verbroekken</td>
		<td><h3>dhr. H.M.G.</td>
		<td><h3>Conci&euml;rge</td>
	</tr>
	<tr>
		<td><h3>Verheyen</td>
		<td><h3>dhr. A.M.N.G.</td>
		<td><h3>Werkvoorbereidend onderwijsassistent</td>
	</tr>
</table>
<?php include '../footer.php'; ?>
