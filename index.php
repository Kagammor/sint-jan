<?php include 'header.php'; ?>
<p class="title" id="welcome">Welkom op de website van Sint-Jan LVO!</p>
			
<h1 class="header">Nieuws</h1>

<div class="news_item">
	<h2 class="title">Sint-Jan informeert</h2> <span class="title_sub" datetime="2012-03-07">19 maart 2012</span>
	<div class="news_article">Op zaterdag 17 maart was op Sint-Jan open dag. Zonder twijfel een geslaagde dag: zeer druk bezocht en veel enthousiasme bij leerlingen en docenten wat de bezoekers duidelijk aansprak.</div>
</div>
		
<div class="news_item">
	<h2 class="title">Sint-Jan informeert</h2> <span class="title_sub" datetime="2012-03-07">7 maart 2012</span>
	<div class="news_article">Op zaterdag 17 maart is op Sint-Jan open dag. Je bent samen met je ouders, broers en zussen van harte welkom tussen 10.00 uur en 13.30 uur.<br />
	Die dag laten we je zien wat we allemaal te bieden hebben... en dat is veel!</div>
</div>

<div class="news_item">
	<h2 class="title">Sint-Jan viert (65 jaar)</h2> <span class="title_sub" datetime="2012-03-07">7 maart 2012</span>
	<div class="news_article">Sint-Jan bestaat 65 jaar, reden voor een re&#252;nie dus! Op zaterdag 29 september 2012 ben je welkom van 16.30 uur tot 22.30 uur.<br />
	Natuurlijk kom jij ook. Meld je dus vanaf 15 maart aan via de button op de homepage!</div>
</div>

<div class="news_item">
	<h2 class="title">Sint-Jan demo't</h2> <span class="title_sub" datetime="2012-03-03">3 maart 2012</span>
	<div class="news_article">Woensdag 8 februari om 14.30 uur en woensdag 15 februari om 13.00 uur en om 14.30 uur zijn er op Sint-Jan weer demolessen geweest.
	Veel leerlingen van de bassisscholen volgden de minilessen. Klik hier voor foto's!</div>
</div>

<div class="news_item">
	<h2 class="title">Sint-Jan lipdubt</h2> <span class="title_sub" datetime="2012-02-08">8 februari 2012</span>
	<div class="news_article">De lipdub is klaar. Wat een enthousiasme bij deze leerlingen, een &#233;chte promotie voor Sint-Jan!<br />
	Wil je meegenieten? Klik dan <a href="http://www.facebook.com/photo.php?v=242452082501861" target="_blank">hier</a>.</div>
</div>

<span id="news_archive"><a href="/sintjan/administratie/archief.php">Archief</a></span>
<?php include 'footer.php'; ?>
