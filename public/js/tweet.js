$(document).ready(function() {
	// Declare Twitter API URL and variables
	var twitter_api_url = 'http://search.twitter.com/search.json?callback=?&q=@bk_sintjan OR from:bk_sintjan&rpp=1';

	$.getJSON(twitter_api_url, function(data) {
		$.each(data.results, function(i, tweet) {
			// Check for feedback
			if(tweet.text !== undefined) {
				// Format date & time
				var monthNames = ["januari", "februari", "maart", "april", "mei", "juni", "juli", "augustus", "september", "oktober", "november", "december"];

				var tweetPost = new Date(tweet.created_at);
				var tweetDate = tweetPost.getDate() + ' ' + monthNames[tweetPost.getMonth()] + ' ' + tweetPost.getFullYear();
				
				var tweetMinute = tweetPost.getMinutes();
				if(tweetPost.getMinutes() < 10) {
					tweetMinute = '0' + tweetPost.getMinutes();	// Add leading zero if minutes is less than 10 for a more natural display
				}
				var tweetTime = tweetPost.getHours() + ':' + tweetMinute;

				// Build html string
				var tweetHtml  = '<div id="tweet_info">' + tweetDate + '&nbsp;&nbsp;' + tweetTime + '&nbsp;&nbsp;' + tweet.from_user + '<\/div>';
				tweetHtml += '<div id="tweet_text">' + tweet.text + '<\/div>';

				// Append html string to header_social div
				$('#header_social').prepend(tweetHtml);
			}
		});
	});
})
