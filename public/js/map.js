map = new OpenLayers.Map('map');
layer = new OpenLayers.Layer.OSM("OSM Map");

/* Add map layer*/
map.addLayer(layer);

/* Define map center */
map.setCenter(
	new OpenLayers.LonLat(5.9288, 50.9332).transform(
		new OpenLayers.Projection("EPSG:4326"),
		map.getProjectionObject()
	), 14
);

/* Add marker layer */
var markers = new OpenLayers.Layer.Markers("Markers");
map.addLayer(markers);

/* Define marker properties */
var size = new OpenLayers.Size(32, 38);
var offset = new OpenLayers.Pixel(-(size.w/2), -size.h);
var icon = new OpenLayers.Icon('/sintjan/public/img/icons/marker.png', size, offset);
/* Define marker */
marker = new OpenLayers.Marker(new OpenLayers.LonLat(5.9288, 50.9332).transform(
			new OpenLayers.Projection("EPSG:4326"),
			map.getProjectionObject()
		), icon);
/* Add marker to map */
markers.addMarker(marker);
