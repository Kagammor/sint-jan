$(function() {
	$(window).scroll(function() {
		if($(this).scrollTop() > 350) {
			$('#toTop').fadeIn(500);	
		} else {
			$('#toTop').fadeOut(500);
		}
	});
 
	$('#toTop').click(function() {
		$('body,html').animate({scrollTop:0}, 300);
	});	
});
