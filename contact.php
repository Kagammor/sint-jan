<?php include 'header.php'; ?>
<h1 class="header">Contact</h1>

<div id="map_container" style="float: right;">
	<div id="map" style="width: 200px; height: 200px; box-shadow: 0 0 2px #666;"></div>
	<span class="title_sub" style="margin: 5px 0 0 0; float: left;">Sint-Jan op de kaart</span>
</div>

<script src="/sintjan/public/js/openlayers.js"></script>
<script src="/sintjan/public/js/map.js"></script>

<h2 class="title_2nd">Bezoekadres</h2>
<span>Amstenraderweg 122, Hoensbroek</p>

<h2 class="title_2nd">Telefoon</h2>
<p>045-5218666</p>

<h2 class="title_2nd">Fax</h2>
<p>045-5232155</p>

<h2 class="title_2nd">Post</h2>
<p>Postbus 189, 6430 AD Hoensbroek</p>

<h2 class="title_2nd">Mail</h2>
<p>info@sintjan-lvo.nl</p>

<br />

<p>Indien u vragen/opmerkingen heeft over de site, kunt u deze mailen naar <a href="mailto:g.zwitserloot@sintjan-lvo.nl">g.zwitserloot@sintjan-lvo.nl</a>.</p>

<br />

<p>Via <a href="http://www.stjan-mr.nl/">http://www.stjan-mr.nl/</a> kunt u ook contact opnemen met de Medezeggenschapsraad van Sint-Jan.</p>
<?php include 'footer.php'; ?>
