<!DOCTYPE html>
<html>
	<head>
		<title>Sint-Jan LVO</title>
		
		<link href="/sintjan/public/css/reset.css" rel="stylesheet" type="text/css" />
		<link href="/sintjan/public/css/style.css" rel="stylesheet" type="text/css" />
		
		<script src="/sintjan/public/js/jquery.js"></script>
		
		<script src="/sintjan/public/js/scroll.js"></script>
		<script src="/sintjan/public/js/tweet.js"></script>
	</head>
	
	<body>
		<div id="header">
			<div class="container">
				<ul id="header_nav">
					<a href="/sintjan"><li>Home</li></a>
					<a href="/sintjan/sintjan"><li>Sint-Jan
						<ul>
							<a href="/sintjan/sintjan/informatie"><li>Informatie</li></a>
							<a href="/sintjan/sintjan/personeel"><li>Personeel</li></a>
							<a href="/sintjan/sintjan/faciliteiten"><li>Faciliteiten</li></a>
							<a href="/sintjan/sintjan/geschiedenis"><li>Geschiedenis</li></a>
							<a href="/sintjan/sintjan/mr"><li>MR</li></a>
						</ul>
					</li></a>
					<a href="/sintjan/onderwijs"><li>Onderwijs
						<ul>
							<a href="/sintjan/onderwijs/vormgeving"><li>Vormgeving</li></a>
							<a href="/sintjan/onderwijs/tto"><li>Tweetalig</li></a>
							<a href="/sintjan/onderwijs/pta"><li>PTA</li></a>
							<a href="/sintjan/onderwijs/begeleiding"><li>Begeleiding</li></a>
							<a href="/sintjan/onderwijs/visie"><li>Visie</li></a>
						</ul>
					</li></a>
					<a href="/sintjan/administratie"><li>Administratie
						<ul>
							<a href="/sintjan/administratie/kalender"><li>Kalender</li></a>
							<a href="/sintjan/administratie/schoolgids"><li>Schoolgids</li></a>
							<a href="/sintjan/administratie/archief"><li>Nieuwsarchief</li></a>
						</ul></a>
					</li>
					<a href="/sintjan/roosters"><li>Roosters
						<ul>
							<a href="/sintjan/roosters/mijn"><li>Mijn rooster</li></a>
							<a href="/sintjan/roosters/klassenrooster"><li>Klassenrooster</li></a>
							<a href="/sintjan/roosters/toetsen"><li>Toetsen</li></a>
							<a href="/sintjan/roosters/examens"><li>Examens</li></a>
						</ul>
					</li></a>
					<a href="/sintjan/media"><li>Media
						<ul>
							<a href="/sintjan/media/fotos"><li>Foto's</li></a>
							<a href="/sintjan/media/videos"><li>Video's</li></a>
						</ul>
					</li></a>
					<a href="/sintjan/contact"><li>Contact</li></a>
				</ul>
				
				<div id="header_logo"><h1>Sint-Jan LVO</h1></div>
				
				<div id="header_photo"></div>
				
				<ul id="header_levels">
					<li>Gymnasium</li>
					<li>Atheneum</li>
					<li>Tweetalig vwo</li>
					<li>Havo</li>
				</ul>
				
				<div id="header_social">
					<div class="social_icon" id="twitter">Twitter</div>
					<div class="social_icon" id="facebook">Facebook</div>
				</div>
			</div>
		</div>
		
		<div id="content_container">
			<div id="sidebar">
				<h1 class="sidebar_head">Foto's</h1>
					<h2 class="sidebar_title">Open dag</h2>
					<img src="/sintjan/public/img/photos/opendag/1_thumb.jpg" id="sidebar_photo" />
					
				<h1 class="sidebar_head">Mededelingen</h1>
					<span>Er zijn momenteel geen mededelingen.</span>
			</div>
			
			<div id="content">
